import React from "react";
import styled from "styled-components";
import "./App.css";
import Header from "./components/NavBar";
import "bootstrap/dist/css/bootstrap.min.css";
import Home from "./components/Views/Home";
import Footer from "./components/Views/Footer";
import CityCont from './components/City/ApiContainer';

import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

const Containers = styled.section`
  width: 100%;
  max-height: 1200px;
  display: flex;
  flex-direction: column;
  @media only screen and (max-width: 768px) {
  }
`;

function App() {

  return (
    <Containers>
      <Router>
        <Header />

        <Switch>
          <Route exact path="/">
            <Home />
          </Route>
          <Route exact path="/Amman">
            <CityCont cityName="Amman" />
          </Route>
          <Route exact path="/Zarqa">
            <CityCont cityName="Zarqa" />
          </Route>
          <Route exact path="/Irbid">
            <CityCont cityName="Irbid" />
          </Route>
          <Route exact path="/Ajloun">
            <CityCont cityName="Ajloun" />
          </Route>
          <Route exact path="/Ajloun">
            <CityCont cityName="Ajloun" />
          </Route>
          <Route exact path="/Al Jīzah">
            <CityCont cityName="Al Jīzah" />
          </Route>
          <Route exact path="/Al Jubayhah">
            <CityCont cityName="Al Jubayhah" />
          </Route>
          <Route exact path="/Al Karak">
            <CityCont cityName="Al Karak" />
          </Route>
          <Route exact path="/Al Karāmah">
            <CityCont cityName="Al Karāmah" />
          </Route>
          <Route exact path="/Aqaba">
            <CityCont cityName="Aqaba" />
          </Route>
          <Route exact path="/Ar Ramthā">
            <CityCont cityName="Ar Ramthā" />
          </Route>
          <Route exact path="/As Salţ">
            <CityCont cityName="As Salţ" />
          </Route>
          <Route exact path="/Aţ Ţafīlah">
            <CityCont cityName="Aţ Ţafīlah" />
          </Route>
          <Route exact path="/Jarash">
            <CityCont cityName="Jarash" />
          </Route>
          <Route exact path="/Mādabā">
            <CityCont cityName="Mādabā" />
          </Route>
          <Route exact path="/Mafraq">
            <CityCont cityName="Mafraq" />
          </Route>
          <Route exact path="/Marka">
            <CityCont cityName="Marka" />
          </Route>
          <Route exact path="/Muḩāfaz̧at Ma‘ān">
            <CityCont cityName="Muḩāfaz̧at Ma‘ān" />
          </Route>
          <Route exact path="/Petra">
            <CityCont cityName="Petra" />
          </Route>
          <Route exact path="/Russeifa">
            <CityCont cityName="Russeifa" />
          </Route>
          <Route exact path="/Tall al Khalīfah">
            <CityCont cityName="Tall al Khalīfah" />
          </Route>
          <Route exact path="/Umm as Summāq">
            <CityCont cityName="Umm as Summāq" />
          </Route>
        </Switch>
      </Router>
      <Footer />
    </Containers>
  );
}

export default App;
