import React from 'react';
import Menu from './Menu'
import {useTransition, animated} from 'react-spring'
import {Link} from 'react-router-dom'

export default function Header(){

    return(
        <div className="App-head ">
        <Link exact to="/" className="Links">
        <span>Weather Bonanza</span>
        </Link>
        <Menu />
        </div>
    );


}

