import React ,{useState} from 'react';

import {Link} from "react-router-dom"


export default function ResponsiveDrawer(props){

    

    return (
        <div className="menuItems ">
        
      
                    
        <ul>
        <li 
              onClick={props.closeMenu}
             
              >
                <Link to="/" className="Links"  >
                    Home
                </Link>
                
            </li>
            <li 
              onClick={props.closeMenu}
             
              >
                <Link to="/Amman" className="Links">
                    Amman Weather
                </Link>
                
            </li>
            <li 
              onClick={props.closeMenu}
             
              >
                <Link to="/Zarqa" className="Links">
                    Zarqa Weather
                </Link>
                
            </li>
            <li 
              onClick={props.closeMenu}
             
              >
                <Link to="/Irbid" className="Links">
                    Irbid Weather
                </Link>
                
            </li>
            <li 
              onClick={props.closeMenu}
             
              >
                <Link to="/Ajloun" className="Links">
                Ajloun Weather
                </Link>
                
            </li><li 
              onClick={props.closeMenu}
             
              >
                <Link to="/Al Jīzah" className="Links">
                Al Jizah Weather
                </Link>
                
            </li><li 
              onClick={props.closeMenu}
             
              >
                <Link to="/Al Jubayhah" className="Links">
                Al Jubayhah Weather
                </Link>
                
            </li><li 
              onClick={props.closeMenu}
             
              >
                <Link to="/Al Karak" className="Links">
                Al Karak Weather
                </Link>
                
            </li><li 
              onClick={props.closeMenu}
             
              >
                <Link to="/Al Karāmah" className="Links">
                Al Karimah Weather
                </Link>
                
            </li><li 
              onClick={props.closeMenu}
             
              >
                <Link to="/Aqaba" className="Links">
                Aqaba Weather
                </Link>
                
            </li><li 
              onClick={props.closeMenu}
             
              >
                <Link to="/Ar Ramthā" className="Links">
                Ar Ramtha Weather
                </Link>
                
            </li><li 
              onClick={props.closeMenu}
             
              >
                <Link to="/As Salţ" className="Links">
                As Salt Weather
                </Link>
                
            </li><li 
              onClick={props.closeMenu}
             
              >
                <Link to="/Aţ Ţafīlah" className="Links">
                At Tafilah Weather
                </Link>
                
            </li><li 
              onClick={props.closeMenu}
             
              >
                <Link to="/Jarash" className="Links">
                Jarash Weather
                </Link>
                
            </li><li 
              onClick={props.closeMenu}
             
              >
                <Link to="/Mafraq" className="Links">
                Mafraq Weather
                </Link>
                
            </li><li 
              onClick={props.closeMenu}
             
              >
                <Link to="/Marka" className="Links">
                Marka Weather
                </Link>
                
            </li><li 
              onClick={props.closeMenu}
             
              >
                <Link to="/Muḩāfaz̧at Ma‘ān" className="Links">
               Ma'an Weather
                </Link>
                
            </li><li 
              onClick={props.closeMenu}
             
              >
                <Link to="/Petra" className="Links">
                Petra Weather
                </Link>
                
            </li><li 
              onClick={props.closeMenu}
             
              >
                <Link to="/Russeifa" className="Links">
                Russeifa Weather
                </Link>
                
            </li><li 
              onClick={props.closeMenu}
             
              >
                <Link to="/Tall al Khalīfah" className="Links">
                Tall al Khalifah Weather
                </Link>
                
            </li><li 
              onClick={props.closeMenu}
             
              >
                <Link to="/Umm as Summāq" className="Links">
                Umm as Summiq Weather
                </Link>
                
            </li>
            
           
            <li 
             onClick={props.closeMenu}>
                <Link to="/About" className="Links">
                    About Us
                </Link>
                </li>
                <li 
             onClick={props.closeMenu}>
                <Link to="/ContactUs" className="Links">
                    Contact Us
                </Link>
                </li> 
               
            </ul>
        </div>);
}
