import React,{useState, useEffect} from 'react';

import styled from 'styled-components';
import TopContainer from '../TopContainer';
import BottomContainer from '../BottomContainer';
import News from '../Views/News';


const MContainer = styled.section
`
max-width:90%;
max-height:1000px;
display:flex;
flex-direction:column;
border:solid 1px white;
box-shadow:0px 0px 1rem black;
margin:auto;
margin-top:10%;
border-radius:3rem;
padding:2%;
margin-bottom:5%;
background-color:#bbd4ff;
@media only screen and (max-height: 1024px){
    @media only screen and (max-width: 768px){
        padding-top:10%;
        margin-top:25%;
        font-weight:bold;
}}
@media only screen and (max-width: 550px){
    }
    @media only screen and (max-width: 768px){
        }
`

export default function CityCont(props){

  
return(
    <>
    <MContainer>
        <TopContainer cityName={props.cityName}/>
        
        <BottomContainer cityName={props.cityName}/>
    </MContainer>
    
    <News />
    </>
)



}