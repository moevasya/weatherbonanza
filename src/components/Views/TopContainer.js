import React from 'react';
import styled from 'styled-components';
import SubTop from './SubTop';

const TopCont = styled.div 
`
display:block;
height:400px;
margin:auto;
padding-top:100px;

    @media only screen and (max-width: 500px){
        padding-top:50px;
        height: 300px;
width:auto;
margin:0px;
    }
  

`

export default function TopContainer(props){

return(
    <TopCont  className="TopCont">
<SubTop cityName={props.cityName}/>
    </TopCont>

)

}