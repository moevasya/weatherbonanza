import React from 'react';
import {Link} from 'react-router-dom'
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import Slider from 'infinite-react-carousel';
import styled from 'styled-components';
import TopContainer from './TopContainer'
import HomeApi from './HomeApi';
import News from './News'

const Containers = styled.section`
  width:100%;
  max-height:1200px;
display:flex;
flex-direction:column;
 @media only screen and (max-width: 768px){
 
 
}
 `
export default function Home(){

    //document.getElementById("body").style.backgroundColor = "White";   


    return (<Containers>
    <Slider  duration="5" className="Carousel">
        <Link to="/Amman" className="MainSlide">
    <div classNAme="AmmanBg">
        <TopContainer cityName="Amman" />
    </div>
    </Link>
    <Link to="/Zarqa" className="MainSlide">
    <div>
    <TopContainer cityName="Zarqa" />
    </div>
    </Link>
    <Link to="/Irbid" className="MainSlide">
    <div>
    <TopContainer cityName="Irbid" />
    </div>
    </Link>
  </Slider>
    
    <News />
    
    
    </Containers>);
}