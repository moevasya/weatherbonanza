import React from 'react';
import styled from 'styled-components';

const NewsCont = styled.section
`

border: 1px solid black;
border-radius: 2rem;
padding:2%;
box-shadow: 0px 0px 1rem black;
color:black;
background-color:#e6bbff;
margin:2rem;
height:300px;
@media only screen and (max-width: 770px){
    height:400px;
    margin-top:0px;
    overflow-y:scroll;
    padding:15px;

}

`
export default function News(){
    return (
        <> 
        <NewsCont>
            <h3> News </h3>
            <h5>Headline</h5>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
            </p>
        </NewsCont>
        </>
    )
}