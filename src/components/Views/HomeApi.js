import React,{useState,useEffect} from 'react';
import axios from 'axios';
import styled from 'styled-components';


const Description = styled.section`
  color: black;
  margin: auto;
  padding-top: 1%;
  height: 50%;
  width: 50%;
  font-size: 400%;
  color: white;
  display:inline;
  text-shadow:0rem 0rem 0.5rem black;
  font-family:monospace;
 
  

`;
const Temp = styled.section`
  color: black;
  margin: auto;
  padding-top: 1%;
  height: 50%;
  width: 50%;
  font-size: 400%;
  color: white;
  display:inline;
  text-shadow:0rem 0rem 0.5rem black;
  margin-left:5%;
  font-family:monospace;
 
`;

const CityInfo = styled.section`
color: black;
margin: auto;
padding-top: 1%;
height: 50%;
width: 100%;
font-size: 400%;
color: white;
display:Block;
text-shadow:0rem 0rem 0.5rem black;
text-align:center;
font-family:monospace;


`;
const DateCont = styled.section`
color: black;
margin: auto;
padding-top: 1%;
height: 50%;
width: 50%;
font-size: 400%;
color: white;
display:inline;
text-shadow:0rem 0rem 1rem black;
margin-left:10%;
font-family:monospace;


`;

const Icon = styled.img
`
margin-left:5%;
margin-top:-3%;
height:80%;
width:15%;
margin-right:1%;
border-radius:10%;
box-shadow:0px 0px 1rem black;
`
export default function HomeApi(props){

    const url = `http://api.openweathermap.org/data/2.5/weather?q=${props.cityName}&appid=ca98617be22b7c8ea4a8d444241a7661`;
    const [city, setCity] = useState(null);
  
    useEffect(() => {
      axios.get(url).then((response) => {
          setCity(response.data);
      });
    }, [url]);
    var d = new Date();

  if (city){
  return (
    <>
         <Icon src={`http://openweathermap.org/img/wn/${city.weather[0].icon}.png`}/>

      <Description>{city.weather[0].main}</Description>

      <Temp>
      {parseInt(city.main["temp"])-270}
        {" "}
        <sup>
          C<sup>&#176;</sup>
        </sup>
      </Temp>
  <DateCont>{d.getDate()} -  {d.getMonth()+1} - {d.getFullYear()} </DateCont>

      <CityInfo>{city.name}, {city.sys["country"]}</CityInfo>
    </>
  );}
    return(<>
    
    
    
    
    </>);


}