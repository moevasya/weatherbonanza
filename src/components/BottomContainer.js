import React,{useState, useEffect} from 'react';
import styled from 'styled-components';
import axios from 'axios';

const BotCont = styled.section
`


max-width:100%;
max-height:60%;
background-color:white;
border-radius: 1rem;
background:transparent;
display:block;
margin:auto;
@media only screen and (max-width: 768px){
  height:auto;
  margin-top:5%;
}
`
export default function BottomContainer(props){
    const url = `https://api.openweathermap.org/data/2.5/weather?q=${props.cityName}&appid=ca98617be22b7c8ea4a8d444241a7661`;
    const [city, setCity] = useState(null);
  
    useEffect(() => {
      axios.get(url).then((response) => {
          setCity(response.data);
      });
    }, [url]);
    if (city){
return (

    <BotCont>
        <table className="tg" >
<colgroup>
<col />
<col />
</colgroup>
<thead>
  <tr>
    <th className="tg-0pky">Real Feel </th>
<th className="tg-0lax"> {parseInt(city.main["feels_like"])-270} <sup>C<sup>&#176;</sup></sup></th>
  </tr>
</thead>
<tbody>
  <tr>
    <td className="tg-0lax">Humidity</td>
<td className="tg-0lax">{city.main["humidity"]}%</td>
  </tr>
  <tr>
    <td className="tg-0lax">Pressure</td>
    <td className="tg-0lax">{city.main["pressure"]} N/m<sup>2</sup></td>
  </tr>
  <tr>
    <td className="tg-0lax">Max Temp</td>
    <td className="tg-0lax">{parseInt(city.main["temp_max"])-270} <sup>C<sup>&#176;</sup></sup></td>
  </tr>
  <tr>
    <td className="tg-0lax">Min Temp</td>
    <td className="tg-0lax">{parseInt(city.main["temp_min"])-270} <sup>C<sup>&#176;</sup></sup></td>
  </tr>
  <tr>
    <td className="tg-0lax"></td>
    <td className="tg-0lax"></td>
  </tr>
</tbody>
</table>
    </BotCont>
)
}
return(<></>)}