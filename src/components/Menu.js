import React ,{useState} from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faBars} from '@fortawesome/free-solid-svg-icons'
import {useTransition, animated} from 'react-spring'
import {Link} from "react-router-dom"
import ResponsiveDrawer from './Drawer'



export default function Menu(){
    const[showMenu, setShowMenu] = useState(false);

    const maskTransitions = useTransition(showMenu, null, {
        from: { position: 'absolute', opacity: 0 },
        enter: { opacity: 1 },
        leave: { opacity: 0 },
    })
    const menuTransitions = useTransition(showMenu, null, {
        from: { opacity:0, transform: 'translatex(-100%)' },
        enter: { opacity: 1, transform: 'translatex(0%)'},
        leave: { opacity: 0, transform: 'translatex(-100%)'},
    })


    return(
       <nav className="nav">
           <span className="menuIcon">
           <FontAwesomeIcon
            icon={faBars} 
            onClick={() => setShowMenu(!showMenu)
           }
            />
            </span>

            {
                maskTransitions.map(({ item, key, props }) =>
                item && <animated.div 
                         key={key} 
                         style={props}
                         className="mask"
                         onClick={() => setShowMenu(false)}>
                    

                </animated.div>
                )
                
                
            }
            {
                menuTransitions.map(({ item, key, props }) =>
                item && <animated.div 
                         key={key} 
                         style={props}
                         className="menu"
                         >
                          <ResponsiveDrawer closeMenu={() => setShowMenu(false)} /> 
                           

                           
                            

                            
                    </animated.div>
                )
                
                
            }
       </nav>
    );
    


}

