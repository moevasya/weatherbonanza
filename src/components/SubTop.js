import React, { useState, useEffect } from "react";
import styled from "styled-components";
import axios from "axios";

const CityInfo = styled.section`
text-align: center;
font-weight:bold;

font-family:monospace;
color: black;
font-size:3rem;
text-shadow:0 0 .15rem white;
@media only screen and (max-width: 768px){
  
}
@media only screen and (max-height: 1024px){
  @media only screen and (max-width: 768px){
    margin-bottom: 15%;
  }}


  @media only screen and (max-height: 570px){
    @media only screen and (max-width: 340px){
      font-size:2rem;
    }}
`;

const Icon = styled.img
`
text-align: center;
text-shadow:0 0 .15rem white;
color: black;

width:3rem;
height 3rem;
display: inline block;
margin:auto;
`

const Description = styled.section`
text-align: center;
font-size:2rem;
text-shadow:0 0 .15rem white;
color: black;

display:inline;
margin:auto;
text-align: center;

  @media only screen and (max-width: 768px){
   
  }
  
  @media only screen and (max-height: 570px){
    @media only screen and (max-width: 340px){
      font-size:1.3rem;
    }}
`;
const Temp = styled.section`
font-size:2rem;
display:inline;
text-shadow:0 0 .15rem white;
color: black;

@media only screen and (max-height: 570px){
  @media only screen and (max-width: 340px){
    font-size:1.3rem;
  }}
  

`;
const C = styled.sup
`
font-size:1.5rem;
text-shadow:0 0 .15rem white;
color: black;
@media only screen and (max-height: 570px){
  @media only screen and (max-width: 340px){
    font-size:1.3rem;
  }}
`

const DateCont = styled.section`
font-size:2rem;
text-shadow:0 0 .15rem white;
color: black;

@media only screen and (max-height: 570px){
  @media only screen and (max-width: 340px){
    font-size:1.3rem;
  }}

`;



export default function SubTop(props) {
  
  if ("geolocation" in navigator) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var lat= position.coords.latitude;
      var lon= position.coords.longitude;
    });
    } else {
      alert("Not Available");
    }
  

  const url = `https://api.openweathermap.org/data/2.5/weather?q=${props.cityName}&appid=ca98617be22b7c8ea4a8d444241a7661`;
  const [city, setCity] = useState(null);

  useEffect(() => {
    axios.get(url).then((response) => {
        setCity(response.data);
    });
  }, [url]);

var d = new Date();
if (city){
  if(city.name.includes("Governorate")){
    var cityName=city.name.replace("Governorate","")
}else{
  var cityName=city.name;
}
  return (
    <>
     <CityInfo>{cityName}, {city.sys["country"]}</CityInfo>
    <Icon src={`https://openweathermap.org/img/wn/${city.weather[0].icon}.png`}/>
     
      <Description>{city.weather[0].main}</Description>

      <Temp>{"  "}
      {parseInt(city.main["temp"])-270}
        {""}</Temp>
        <C>
          C<sup>&#176;</sup>
        </C>
      
  <DateCont>{d.getDate()} -  {d.getMonth()+1} - {d.getFullYear()} </DateCont>

     
    </>
  );}

  return(<div></div>);
}
